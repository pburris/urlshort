package urlshort

import (
	"github.com/boltdb/bolt"
)

type Store struct {
	BucketName string
	File       string
	db         *bolt.DB
}

type Url struct {
	Path string `json:"path" yaml:"path"`
	Url  string `json:"url" yaml:"url"`
}

func (s *Store) WriteUrl(url Url) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(s.BucketName))
		return b.Put([]byte(url.Path), []byte(url.Url))
	})
}

func (s *Store) ReadUrl(path string) (*string, error) {
	var ret string
	err := s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(s.BucketName))
		data := b.Get([]byte(path))
		ret = string(data)
		return nil
	})
	return &ret, err
}

func NewStore(file, bucket string, db *bolt.DB) (*Store, error) {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return &Store{
		BucketName: bucket,
		File:       file,
		db:         db,
	}, nil
}
