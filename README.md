# URL Shortener

Usage:
```
Usage of urlshort:
  -bucket string
    	name of the boltdb bucket to use (default "MainBucket")
  -db string
    	path to the boltdb database file (default "urlshort.db")
  -port string
    	the port that the http server runs on (default "8080")
```

URL Shortener uses BoltDB as the storage medium.

# Running

You can run `make build-docker` to build the local docker image. You can then use `make docker-run` to run the built image.


# Testing

there is a script attached used to add items to the database
