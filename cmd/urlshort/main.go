package main

import (
	"flag"
	"log"

	"github.com/boltdb/bolt"
	"github.com/jumballaya/urlshort"
)

func main() {
	port := flag.String("port", "8080", "the port that the http server runs on")
	database := flag.String("db", "urlshort.db", "path to the boltdb database file")
	bucket := flag.String("bucket", "MainBucket", "name of the boltdb bucket to use")
	flag.Parse()

	db, err := bolt.Open(*database, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	server, err := urlshort.NewServer(*port, *database, *bucket, db)

	if err != nil {
		log.Fatal(err)
	}

	server.Start()
}
