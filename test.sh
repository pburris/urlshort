#!/bin/bash

#
# Usage: ./test.sh 3000 /testing http://www.example.com
#
#                   ^      ^              ^
#                  PORT   PATH           URL
#
#

curl -X POST http://localhost:$1/new -d "path=$2&url=$3"
