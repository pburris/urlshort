# Builder
FROM golang as builder
WORKDIR /go/src/github.com/jumballaya/urlshort
COPY . .
RUN make build-linux

# Runner
FROM debian:stretch
WORKDIR /bin/
COPY --from=builder /go/src/github.com/jumballaya/urlshort/dist/urlshort_unix ./urlshort
CMD ["urlshort"]
