package urlshort

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/boltdb/bolt"
)

type Server struct {
	Port  string
	Mux   *http.ServeMux
	Store *Store
}

func NewServer(port, file, bucket string, db *bolt.DB) (*Server, error) {
	store, err := NewStore(file, bucket, db)
	if err != nil {
		return nil, err
	}

	server := &Server{
		Port:  port,
		Store: store,
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", homeRoute)
	mux.HandleFunc("/new", server.newUrlRoute())
	server.Mux = mux

	return server, nil
}

func (s *Server) Start() {
	fmt.Println(fmt.Sprintf("Starting server on port %s", s.Port))
	err := http.ListenAndServe(":"+s.Port, s.handler())
	if err != nil {
		log.Fatal(err)
	}
}

func (s *Server) handler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		url, err := s.Store.ReadUrl(path)
		if err != nil || url == nil || *url == "" {
			s.Mux.ServeHTTP(w, r)
			return
		}
		http.Redirect(w, r, *url, http.StatusFound)
	}
}

func homeRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "<h1>URL Shortener</h1>")
}

func (s *Server) newUrlRoute() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			fmt.Fprintln(w, fmt.Sprintf("Method: %s not supported for this route", r.Method))
			return
		}
		r.ParseForm()
		var url Url

		for k, v := range r.Form {
			if k == "path" {
				url.Path = strings.Join(v, "")
			}
			if k == "url" {
				url.Url = strings.Join(v, "")
			}
		}
		if url.Path != "" && url.Url != "" {
			s.Store.WriteUrl(url)
		}
		fmt.Fprintln(w, "<h1>URL Created</h1>")
	}
}
